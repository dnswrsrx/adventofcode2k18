import re
import string

def get_step(line):
    return re.findall('step (\w)', line)[0]

def get_dependency(line):
    return re.findall('Step (\w)', line)[0]

def make_step_dependency_dict(instructions):
    step_dependency_dict = {}
    for line in instructions:
        if get_step(line) in step_dependency_dict:
            step_dependency_dict[get_step(line)].append(get_dependency(line))
        else:
            step_dependency_dict[get_step(line)] = [get_dependency(line)]
    return step_dependency_dict

def get_steps_with_dependencies(step_dependency_dict):
    return list(set(step_dependency_dict.keys()))

def get_dependencies(step_dependency_dict):
    return list(set(dependency for values in step_dependency_dict.values() for
            dependency in values))

def get_steps_without_dependencies(step_dependency_dict):
    return [dependency for dependency in
            get_dependencies(step_dependency_dict) if dependency not in
            get_steps_with_dependencies(step_dependency_dict)]

def get_all_steps(step_dependency_dict):
    return(list(set(get_steps_with_dependencies(step_dependency_dict)
        + get_dependencies(step_dependency_dict))))

def check_for_dependencies_in_built(dependencies, built):
    for dependency in dependencies:
        if dependency not in built:
            return False
    return True

def update_to_build(step_dependency_dict, built, to_build):
    return to_build + [step for step in
            get_steps_with_dependencies(step_dependency_dict)
            if check_for_dependencies_in_built(step_dependency_dict[step], built)
            and step not in to_build and step not in built]

def build(to_build, built, all_steps):
    if to_build != []:
        built.append(sorted(to_build)[0])
        to_build.remove(built[-1])
    if all_steps != []:
        all_steps.remove(built[-1])
    return built, to_build, all_steps

def main_part_one(instructions):
    step_dependency_dict = make_step_dependency_dict(instructions)
    to_build = get_steps_without_dependencies(step_dependency_dict)
    all_steps = get_all_steps(step_dependency_dict)
    built = []
    while all_steps:
        to_build = update_to_build(step_dependency_dict, built, to_build)
        built, to_build, all_steps = build(to_build, built, all_steps)
    return ''.join(built)


def make_alphabets_to_time_dict(constant):
    return {alphabet: constant+time for time, alphabet in
            enumerate(string.ascii_uppercase)}

class Elf:

    def __init__(self, constant):
        self.alphabet = None
        self.time_remaining = 0
        self.time_constant = constant + 1

    def start_alphabet(self, alphabet):
        self.alphabet = alphabet
        self.time_remaining = make_alphabets_to_time_dict(self.time_constant)[alphabet]

    def run_job(self):
        self.time_remaining -= 1

    def remove_alphabet(self):
        self.alphabet = None
        self.time_remaining = 0


class Manager:

    def __init__(self, number_of_elves, constant):
        self.elves = [Elf(constant) for i in range(number_of_elves)]

    def get_elves_tasks(self):
        return (elf.alphabet for elf in self.elves)

    def assign_alphabet_from_to_build(self, to_build):
        for alphabet in sorted(to_build):
            for elf in self.elves:
                if not elf.alphabet and alphabet not in self.get_elves_tasks():
                    elf.start_alphabet(alphabet)

    def run_iteration(self, built, all_steps, to_build):
        for elf in self.elves:
            if elf.alphabet:
                if elf.time_remaining > 1:
                    elf.run_job()
                else:
                    built.append(elf.alphabet)
                    all_steps.remove(elf.alphabet)
                    to_build.remove(elf.alphabet)
                    elf.remove_alphabet()
        return built, all_steps, to_build


def main_part_two(instructions, number_of_elves, time_constant):
    step_dependency_dict = make_step_dependency_dict(instructions)
    to_build = get_steps_without_dependencies(step_dependency_dict)
    all_steps = get_all_steps(step_dependency_dict)
    built = []
    manager = Manager(number_of_elves, time_constant)
    counter = 0
    while all_steps:
        manager.assign_alphabet_from_to_build(to_build)
        built, all_steps, to_build = manager.run_iteration(built, all_steps,
                to_build)
        to_build = update_to_build(step_dependency_dict, built, to_build)
        counter += 1
    return counter


if __name__ == '__main__':
    with open('./input/day7input.txt', 'r') as input_file:
        instructions = input_file.read().splitlines()
    print('The answer to part 1: ', main_part_one(instructions))
    print('The answer to part 2: ', main_part_two(instructions,
        number_of_elves=5, time_constant=60))
