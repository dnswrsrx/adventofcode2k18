import operator

class Danger_Point:

    def __init__(self, id, line):
        self.id = id
        self.x = int(line.split(', ')[0])
        self.y = int(line.split(', ')[1])
        self.infinite = False
        self.closest_cells = []


class Cell:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.closest_danger_point = 0
        self.total_distance_to_all_points = 0


class Grid:

    def __init__(self, danger_points):
        self.danger_points = danger_points
        self.top = self.identify_borders()['top']
        self.bottom = self.identify_borders()['bottom']
        self.left = self.identify_borders()['left']
        self.right = self.identify_borders()['right']
        self.grid = self.make_grid()

    def identify_borders(self):
        return {'left': min(point.x for point in self.danger_points),
               'right': max(point.x for point in self.danger_points)+1,
               'top': min(point.y for point in self.danger_points),
               'bottom': max(point.y for point in self.danger_points)+1}

    def make_grid(self):
        return [[Cell(x, y) for x in range(self.left, self.right)] for y in
                range(self.top, self.bottom)]

    def cell_at(self, x, y):
        x_converted = x - self.left
        y_converted = y - self.top
        return self.grid[x_converted][y_converted]

    def distance_between(self, cell, danger_point):
        x_difference = abs(cell.x - danger_point.x)
        y_difference = abs(cell.y - danger_point.y)
        return x_difference + y_difference

    def get_closest_danger_points(self, cell):
        minimum_distance =  min(self.distance_between(cell, point) for point in
                self.danger_points)
        closest_danger_points = [point for point in self.danger_points if
                self.distance_between(cell, point) == minimum_distance]
        if len(closest_danger_points) == 1:
            return closest_danger_points[0]
        else:
            return 0

    def assign_closest_danger_points_to_cells(self):
        for row in self.grid:
            for cell in row:
                cell.closest_danger_point = self.get_closest_danger_points(cell)
        return self

    def assign_closest_cells_to_danger_points(self):
        for point in self.danger_points:
            point.closest_cells = [cell for row in self.grid for cell in row if
                    point is cell.closest_danger_point]
        return self

    def assign_infinite_points_as_infinite(self):
        for point in self.danger_points:
            for cell in point.closest_cells:
                if cell.x in [self.left, self.right] or cell.y in [self.top,
                    self.bottom]:
                    point.infinite = True
        return self

    def obtain_largest_area_for_finite_danger_points(self):
        return max(len(point.closest_cells) for point in self.danger_points if
                not point.infinite)

    def total_distance_to_all_points(self, cell):
        return sum(self.distance_between(cell, point) for point in
                self.danger_points)

    def assign_cells_with_total_distance_to_all_points(self):
        for row in self.grid:
            for cell in row:
                cell.total_distance_to_all_points =\
                self.total_distance_to_all_points(cell)
        return self

    def count_cells_with_total_distance_under_limit(self):
        return sum(1 if cell.total_distance_to_all_points < 10000 else 0 for
                row in self.grid for cell in row)


if __name__ == '__main__':
    with open('./input/day6input.txt', 'r') as input_file:
        input_file = [line.strip() for line in input_file]
    danger_points = [Danger_Point(id, line) for id, line in enumerate(input_file)]
    grid = Grid(danger_points)
    grid.assign_closest_danger_points_to_cells()
    grid.assign_closest_cells_to_danger_points()
    grid.assign_infinite_points_as_infinite()
    print('The answer to part 1: %s' % grid.obtain_largest_area_for_finite_danger_points())
    grid.assign_cells_with_total_distance_to_all_points()
    print('The answer to part 2: %s' % grid.count_cells_with_total_distance_under_limit())
