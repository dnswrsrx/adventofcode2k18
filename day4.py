import re
import dateutil.parser

class Guard:

    def __init__(self, id):
        self.id = id
        self.fall_asleep = []
        self.wake_up = []
        self.sleep_total = 0
        self.count_of_minutes_asleep = None

    def get_minute_start_asleep(self, entry):
        self.fall_asleep.append(
            dateutil.parser.parse(re.findall('\[(.*?)\]', entry)[0]).minute)
        return self

    def get_minute_awake(self, entry):
        self.wake_up.append(
            dateutil.parser.parse(re.findall('\[(.*?)\]', entry)[0]).minute)
        return self

    def get_total_time_asleep(self):
        return sum([time_awake - time_asleep for time_asleep,
            time_awake in zip(self.fall_asleep, self.wake_up)])

    def list_minutes_asleep(self):
        return [minute for time_asleep, time_awake in
                zip(self.fall_asleep, self.wake_up) for minute in
                range(time_asleep, time_awake)]

    def get_count_of_minutes_asleep(self, list_of_minutes_asleep):
        return {minute: list_of_minutes_asleep.count(minute) for minute in
                list_of_minutes_asleep}


class Records:

    def __init__(self):
        self.guard_ids = {}

    def get_guard_id(self, entry):
        return int(re.findall('#(\d+)', entry)[0])

    def parse_log(self, record):
        current_guard_id = None
        for entry in record:
            if 'Guard' in entry:
                current_guard_id = self.get_guard_id(entry)
                if current_guard_id not in self.guard_ids:
                    self.guard_ids[current_guard_id] = Guard(current_guard_id)
            elif 'falls asleep' in entry:
                self.guard_ids[current_guard_id].get_minute_start_asleep(entry)
            elif 'wakes up' in entry:
                self.guard_ids[current_guard_id].get_minute_awake(entry)
        return self

    def assign_guards_total_sleep_times(self):
        for id in self.guard_ids:
            self.guard_ids[id].sleep_total =\
            self.guard_ids[id].get_total_time_asleep()
        return self

    def assign_guards_count_of_minutes_asleep(self):
        for id in self.guard_ids:
            self.guard_ids[id].count_of_minutes_asleep =\
            self.guard_ids[id].get_count_of_minutes_asleep(
                self.guard_ids[id].list_minutes_asleep())
        return self

    def get_guard_times_most_minute_asleep(self):
        guard_with_max_sleep_time = max(((id, self.guard_ids[id].sleep_total)
            for id in self.guard_ids), key=lambda x: x[1])[0]
        max_minute_asleep =\
            max(self.guard_ids[guard_with_max_sleep_time].count_of_minutes_asleep,
                key = lambda minute:
                self.guard_ids[guard_with_max_sleep_time].count_of_minutes_asleep[minute])
        return(guard_with_max_sleep_time * max_minute_asleep)

    def get_guard_times_most_frequent_asleep_minute(self):
        max_asleep_minute_frequency =\
        max([frequency for id in self.guard_ids for frequency in
            list(self.guard_ids[id].count_of_minutes_asleep.values())])
        most_frequent_asleep_minute_and_guard = [(id, minute)
                for id in self.guard_ids
                for minute in self.guard_ids[id].count_of_minutes_asleep
                if self.guard_ids[id].count_of_minutes_asleep[minute] ==
                max_asleep_minute_frequency][0]
        return(most_frequent_asleep_minute_and_guard[0] *
                most_frequent_asleep_minute_and_guard[1])



if __name__ == '__main__':
    with open('./input/day4input.txt', 'r') as record:
        record = sorted([entry.strip('\n') for entry in record if entry])
    #print(input_file)
    records = Records()
    records.parse_log(record)
    records.assign_guards_total_sleep_times()
    records.assign_guards_count_of_minutes_asleep()
    records.get_guard_times_most_frequent_asleep_minute()
    print('The answer to part 1: %s' %
            (records.get_guard_times_most_minute_asleep()))
    print('The answer to part 2: %s' %
            (records.get_guard_times_most_frequent_asleep_minute()))
