import itertools

def main(input_file):
    game = Game(input_file.read())
    game.run_game()
    output = """Results for part 1: {first}, Results for part 2:{second}""".format(
            **game.results)
    return output

def parse_input(input_file):
    numbers = [int(word) for word in input_file.split() if word.isdigit()]
    return {'players': numbers[0], 'last marble': numbers[1]}

class Game:

    def __init__(self, input_file):
        self.players = [Player() for id in
                range(parse_input(input_file)['players'])]
        self.last_marble_1 = parse_input(input_file)['last marble']
        self.last_marble_2 = self.last_marble_1 * 100
        self.current_marble = Marble(0)
        self.results = {"first": None, "second": None}

    def initialise_board(self):
        self.current_marble.left = self.current_marble
        self.current_marble.right = self.current_marble

    def run_game(self):
        self.initialise_board()
        for marble, player in zip(
                range(1, self.last_marble_2),
                itertools.cycle(self.players)
        ):
            if marble == self.last_marble_1:
                self.results["first"] = self.get_highest_score_from_players()
            if marble % 23 == 0:
                self.update_marble_values_if_value_divisible_by_23(player, marble)
            else:
                self.update_marble_values_regularly(marble)
        self.results["second"] = self.get_highest_score_from_players()


    def update_marble_values_regularly(self, marble):
        self.current_marble = Marble(marble, self.current_marble.right,
                self.current_marble.right.right)
        self.current_marble.left.right = self.current_marble
        self.current_marble.right.left = self.current_marble

    def update_marble_values_if_value_divisible_by_23(self, player, marble):
        self.marble_to_player(player, Marble(marble))
        self.change_current_marble(clockwise=False, number=7)
        self.marble_to_player(player, self.current_marble)
        self.remove_marble(self.current_marble)
        self.change_current_marble(clockwise=True, number=1)

    def change_current_marble(self, clockwise=True, number=0):
        if clockwise:
            for _ in range(number):
                self.current_marble = self.current_marble.right
        else:
            for _ in range(number):
                self.current_marble = self.current_marble.left

    def remove_marble(self, marble):
        marble.left.right = marble.right
        marble.right.left = marble.left

    def marble_to_player(self, player, marble):
        player.append_marble(marble)

    def get_highest_score_from_players(self):
        return max([player.sum_marble_values() for player in self.players])


class Marble:

    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


class Player:

    def __init__(self):
        self.marbles = []

    def append_marble(self, marble):
        self.marbles.append(marble)

    def sum_marble_values(self):
        return sum([marble.value for marble in self.marbles])

if __name__ == '__main__':
    with open('./input/day9input.txt', 'r') as input_file:
        print(main(input_file))
