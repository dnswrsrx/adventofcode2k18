def parse_file(input_file):
    return [line.strip() for line in input_file if line.strip()]

def check_for_repetition(line, repetition):
    for character in set(line):
        if line.count(character) == repetition:
            return True
    return False

def checksum(lines):
    doubles = [line for line in lines if check_for_repetition(line, 2)]
    triples = [line for line in lines if check_for_repetition(line, 3)]
    return len(doubles) * len(triples)

def return_matching_tuples(line_one, line_two):
    list_of_tuples = [pair for pair in zip(line_one, line_two) if pair[0] ==
            pair[1]]
    return list_of_tuples

def return_characters_of_lines_with_one_unequal_tuple(lines):
    for line_one in lines:
        for line_two in lines:
            if len(return_matching_tuples(line_one, line_two)) == len(line_one)-1:
                output = [pair[0] for pair in return_matching_tuples(line_one,
                    line_two)]
    return output

if __name__ == '__main__':
    with open('./input/day2input.txt', 'r') as input_file:
        lines = parse_file(input_file)
    print('Answer to part 1: ', checksum(lines))
    print(return_characters_of_lines_with_one_unequal_tuple(lines))
