def get_previous_index(current_index, reacted_indices):
    initial_previous_index = current_index - 1
    while initial_previous_index in reacted_indices:
        initial_previous_index -= 1
    return initial_previous_index

def compile_list_of_reacted_indices(polymer):
    reacted_indices = []
    for i in range(1, len(polymer)):
        previous_index = get_previous_index(i, reacted_indices)
        if correct_polarity(polymer[previous_index], polymer[i]):
            reacted_indices.extend([previous_index, i])
    return reacted_indices

def length_of_list_minus_reacted_indices(polymer, reacted_indices):
    return len(polymer) - len(reacted_indices)


def correct_polarity(first_character, second_character):
    return(first_character.islower() and second_character.isupper()
        and first_character == second_character.lower()
        or
        first_character.isupper() and second_character.islower()
        and first_character == second_character.upper())

def get_first_match_indices(polymer):
    for i in range(len(polymer)-1):
        if correct_polarity(polymer[i], polymer[i+1]):
            return i
    else:
        return 'No matches'

def delete_at(list_of_characters, index):
    for _ in range(2):
        list_of_characters.pop(index)
    return list_of_characters

def pop_matches(polymer):
    polymer = [character for character in polymer]
    while get_first_match_indices(polymer) != 'No matches':
        polymer = delete_at(polymer, get_first_match_indices(polymer))
    return len(polymer)


def polymer_after_alphabet_pop(polymer, alphabet):
    return polymer.replace(alphabet, '').replace(alphabet.upper(), '')

def compile_dictionary_of_alphabet_and_length_after_processing(polymer):
    return {alphabet: pop_matches(polymer_after_alphabet_pop(polymer, alphabet))
            for alphabet in set(polymer.lower())}

def return_minimum_value(dictionary_of_alphabet_and_length):
    return min(dictionary_of_alphabet_and_length.values())


if __name__ == '__main__':
    with open('./input/day5input.txt', 'r') as polymer:
        polymer = polymer.read().strip()
    print('Answer to part 1: %s' % pop_matches(polymer))
    print('Answer to part 2: %s' % return_minimum_value(
        compile_dictionary_of_alphabet_and_length_after_processing(polymer)))
