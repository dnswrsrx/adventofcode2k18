import itertools

def parse_input(input_file):
    return [int(item.strip()) for item in input_file.readlines() if item.strip()]

def sum_and_check_for_doubles_other(list_of_numbers):
    history = {0}
    for number in itertools.accumulate(itertools.cycle(list_of_numbers)):
        if number in history:
            return number
        else:
            history.add(number)

if __name__ == '__main__':
    with open('./input/day1input.txt', 'r') as input_file:
        list_of_numbers = parse_input(input_file)
    part_1_output = sum(list_of_numbers)
    part_2_output =sum_and_check_for_doubles_other(list_of_numbers)

    print('The answer to part 1 is %s' % part_1_output)
    print('The answer to part 2 is %s' % part_2_output)
