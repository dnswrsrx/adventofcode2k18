def parse_input(input_file):
    return [int(data) for data in input_file.read().split()]


class Node:

    def __init__(self, numbers):
        self.number_of_children = numbers[0]
        self.constant_number_of_children = numbers[0]
        self.number_of_metadata = numbers[1]
        self.remaining_numbers = numbers[2:]
        self.children = []
        self.metadata = []

    def assign_metadata_and_children(self):
        if self.number_of_children == 0:
            self.append_metadata()
            self.update_remaining_numbers(self.remaining_numbers[self.number_of_metadata:])
        else:
            self.add_children()
            self.children[-1].assign_metadata_and_children()
            self.update_remaining_numbers(self.children[-1].remaining_numbers)
            self.number_of_children -= 1
            self.assign_metadata_and_children()

    def add_children(self):
        self.children.append(Node(self.remaining_numbers))

    def append_metadata(self):
        for number in self.remaining_numbers[:self.number_of_metadata]:
            self.metadata.append(number)

    def update_remaining_numbers(self, remaining_numbers):
        self.remaining_numbers = remaining_numbers

    def collect_all_metadata(self):
        metadata = self.metadata
        for child in self.children:
            metadata += child.collect_all_metadata()
        return metadata

    def second_check(self):
        value = 0
        if self.constant_number_of_children == 0:
            value = sum(self.metadata)
            return value
        else:
            for number in self.metadata:
                if number <= len(self.children):
                    value += self.children[number-1].second_check()
            return value


def main_part_one(numbers):
    node = Node(numbers)
    node.assign_metadata_and_children()
    return sum(node.collect_all_metadata())

def main_part_two(numbers):
    node = Node(numbers)
    node.assign_metadata_and_children()
    return node.second_check()



if __name__ == '__main__':
    with open('./input/day8input.txt', 'r') as input_file:
        numbers = parse_input(input_file)
    print('The answer to part 1: %s' % main_part_one(numbers))
    print('The answer to part 2: %s' % main_part_two(numbers))
