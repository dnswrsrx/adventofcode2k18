const { readFileSync } = require("fs")

let rawBoxIDS = readFileSync("day2.txt", "utf-8").split("\n");
let boxIDS = rawBoxIDS.filter(id => id);

/**
 * Functions for first half of puzzle.
 */
function computeChecksum(boxIDS){
	const occurrences = {twos: 0, threes: 0};
	const occurrencesOfTwosAndThrees = checkForTwosAndThrees.bind(occurrences)
	boxIDS.forEach(id => occurrencesOfTwosAndThrees(id));
	return occurrences.twos * occurrences.threes;
}

function checkForTwosAndThrees(id){
	let stopCheckingForTwos = false;
	let stopCheckingForThrees = false;
	characters = new Set(id)
	for (character of characters){
		if (checkForOccurences(id, character, 2) && !stopCheckingForTwos){
			this.twos += 1;
			stopCheckingForTwos = true;
		} else if (checkForOccurences(id, character, 3) &&
			!stopCheckingForThrees){
			this.threes += 1;
			stopCheckingForThrees = true;
		}

		if (stopCheckingForTwos && stopCheckingForThrees){
			break;
		}
	}
}

function checkForOccurences(id, character, numberOfOccurrences){
	return countOccurence(id, character) === numberOfOccurrences;
}

function countOccurence(id, character){
	return id.split(character).length - 1;
}

console.log(computeChecksum(boxIDS));

for (indexOne in boxIDS){
	for (let indexTwo=parseInt(indexOne)+1; indexTwo<boxIDS.length; indexTwo++){
		let sameCharacters = new Array();
		for (index in boxIDS[indexOne]){
			if (boxIDS[indexOne][index] === boxIDS[indexTwo][index]){
				sameCharacters.push(boxIDS[indexOne][index]);
			}
		}
		if (sameCharacters.length === Array.from(boxIDS[indexOne]).length-1){
			console.log(sameCharacters.toString().replace(/,/gi, ""));
			break;
		}
	}
}
