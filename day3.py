import re

def parse_input(input_file):
    return (line.strip('\n') for line in input_file.readlines() if
            line.strip('\n'))

def get_coordinates_and_claims(claims):
    coordinates_and_claims = {}
    for claim in claims:
        claim_id = get_id(claim)
        starting_x, starting_y = get_starting_coordinates(claim)
        x_travel, y_travel = get_travel(claim)
        for x in range(starting_x, starting_x+x_travel):
            for y in range(starting_y, starting_y+y_travel):
                coordinate = (x, y)
                if coordinate in coordinates_and_claims:
                    coordinates_and_claims[coordinate].append(claim_id)
                else:
                    coordinates_and_claims[coordinate] = [claim_id]
    return coordinates_and_claims

def get_starting_coordinates(claim):
        starting_x, starting_y = re.findall('(\d+),(\d+)', claim)[0]
        return (int(starting_x), int(starting_y))

def get_travel(claim):
        x_travel, y_travel = re.findall('(\d+)x(\d+)', claim)[0]
        return (int(x_travel), int(y_travel))

def get_id(claim):
    return re.findall('#(\d+)', claim)[0]

def get_coordinates_with_two_plus_claims(coordinates_and_claims):
    return list(filter(
        lambda coordinate: len(coordinates_and_claims[coordinate])>1,
        coordinates_and_claims
        )
    )

def get_id_with_no_overlapping_claims(
    coordinates_and_claims,
    coordinates_with_two_plus_claims
):
    id_with_overlapping_claims = get_id_with_overlapping_claims(
        coordinates_and_claims,
        coordinates_with_two_plus_claims
    )
    coordinates_with_one_claim = get_coordinates_with_one_claim(
        coordinates_and_claims,
        coordinates_with_two_plus_claims
    )
    for id in coordinates_with_one_claim.values():
        if id[0] not in id_with_overlapping_claims:
            return id[0]

def get_id_with_overlapping_claims(
    coordinates_and_claims,
    coordinates_with_two_plus_claims
):
    ids = set()
    for coordinate in coordinates_with_two_plus_claims:
        for id in coordinates_and_claims[coordinate]:
            ids.add(id)
    return ids

def get_coordinates_with_one_claim(
    coordinates_and_claims,
    coordinates_with_two_plus_claims
):
    new_coordinates_and_claims = coordinates_and_claims.copy()
    for coordinate in coordinates_with_two_plus_claims:
        del new_coordinates_and_claims[coordinate]
    return new_coordinates_and_claims

if __name__ == "__main__":
    with open('./input/day3input.txt', 'r') as input_file:
        claims = parse_input(input_file)

    coordinates_and_claims = get_coordinates_and_claims(claims)
    coordinates_with_two_plus_claims = get_coordinates_with_two_plus_claims(
        coordinates_and_claims
    )
    print(len(coordinates_with_two_plus_claims))
    print(get_id_with_no_overlapping_claims(coordinates_and_claims,
        coordinates_with_two_plus_claims))
