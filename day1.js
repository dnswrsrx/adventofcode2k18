const { readFileSync } = require("fs")

let frequencies = readFileSync("day1.txt", "utf-8").split("\n");
let frequenciesInt = frequencies
	.filter(frequency => frequency)
	.map(frequency => parseInt(frequency));

console.log(frequenciesInt
	.reduce((accumulator, currentValue) => accumulator + currentValue));

let total = 0;
let historyOfTotal = new Set();
let runningIndex = 0;
const index = (runningIndex) => runningIndex % frequenciesInt.length;

while (!historyOfTotal.has(total)){
	historyOfTotal.add(total);
	total += frequenciesInt[index(runningIndex)];
	runningIndex += 1;
}
console.log(total);
